module ComponentHelper
  def filename( string )
    string.split('.').first
  end

  def pattern_id( key, pattern )
    "#{key}_#{filename(pattern)}".downcase
  end
end
