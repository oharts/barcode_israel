class ComponentController < ApplicationController

  layout 'component'

  def index
    fetch_patterns(["atoms", "molecules", "organisms", "pages", "templates"])
  end

  def show
    fetch_patterns([params[:id]])
    @pattern = params[:id].capitalize
  end

  def component_item
    @partial = "component/#{params[:type]}/#{params[:id]}"
    @sourcepartial = File.open("#{Rails.root}/app/views/component/#{params[:type]}/_#{params[:id]}.html.erb").read
    @pattern = params[:id].capitalize
  end

  private

  def fetch_patterns(groups)
    @pattern_groups = groups
    @patterns = {}
    @pattern_groups.each do |dir|
      group = @patterns[dir] = []
      fetch_files(dir).sort.each do |method|
        string = method.split("/").last
        string[0] = ""
        group << string
      end
    end
    @patterns
  end

  def fetch_files(dir)
    Dir.glob("#{Rails.root}/app/views/component/#{dir}/*.erb")
  end

end
